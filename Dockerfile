FROM projectdiscovery/nuclei:v2.8.8

ARG TARGETOS
ARG TARGETARCH

RUN apk update
RUN apk upgrade
RUN apk add bash

ADD ./docker-entrypoint.sh ./docker-entrypoint.sh
ADD ./build/nuclei-analyzer-${TARGETOS}-${TARGETARCH} ./nuclei-analyze

ENTRYPOINT ["./docker-entrypoint.sh"]
