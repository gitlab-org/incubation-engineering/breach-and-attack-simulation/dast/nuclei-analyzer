# nuclei-analyzer

:warning: Wondering why the container registry shows this image as 0 bytes? See this comment around buildx compatability https://gitlab.com/gitlab-org/gitlab/-/issues/341939#note_725004999.

## Description

nuclei-analyzer converts [nuclei](https://github.com/projectdiscovery/nuclei) JSONL(ines) output into the appropriate GitLab [DAST security report](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsdast) format.

The nuclei-analyzer Docker image invokes nuclei and nuclei-analyzer using a docker-entrypoint to act as a [Security scanner integration](https://docs.gitlab.com/ee/development/integrations/secure.html) for GitLab CI/CD.

## Getting started

nuclei-analyzer requires at least **go1.19** to install successfully. Run the following to install the latest version:

```
go install -v gitlab.com/gitlab-org/incubation-engineering/breach-and-attack-simulation/dast/nuclei-analyzer@latest
```

`nuclei-analyzer` can be invoked from `$GOPATH/bin`.

The nuclei-analyzer Docker image uses an entry point which runs nuclei for you:

```
# Caution! This image is damn vulnerable do not run this on accessible networks.
docker run --rm -d --name dvwa -p 8008:80 -it vulnerables/web-dvwa:1.9

# Run nuclei and nuclei-analyzer.
docker run --rm --name nuclei-analyzer -it registry.gitlab.com/gitlab-org/incubation-engineering/breach-and-attack-simulation/dast/nuclei-analyzer:latest -u http://host.docker.internal:8008 -tags dvwa,php -json

# Force kill the example vulnerable image.
docker rm -f dvwa
```
