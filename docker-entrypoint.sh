#!/bin/sh

usage()
{
  echo "Usage: $0 [nuclei opts]" >&2
  exit 2
}

if [ -z "$DAST_WEBSITE" ]; then
  echo "DAST_WEBSITE must be set" >&2
  usage
fi

export TARGET=$(cat /etc/hosts | awk '{if ($2 == "'$DAST_WEBSITE'") print $1;}')
if [[ -z "$TARGET" ]]; then
  echo "Unable to resolve dvwa service to an IP" >&2
  exit 1
else
  echo "dvwa service running at $TARGET" >&2
fi

NUCLEI_ARGS="-target $TARGET -v -vv -headless -show-match-line -include-rr -json"
[[ -n "$NUCLEI_TEMPLATES" ]] && NUCLEI_ARGS="$NUCLEI_ARGS -templates $NUCLEI_TEMPLATES"
echo ls -al >&2
ls -al >&2
echo ls $NUCLEI_TEMPLATES >&2
ls $NUCLEI_TEMPLATES >&2
echo args:: $NUCLEI_ARGS >&2
echo pwd:: $(pwd) >&2
echo "nuclei $NUCLEI_ARGS > nuclei-results.json" >&2
nuclei $NUCLEI_ARGS > nuclei-results.json
if [ 0 -ne $? ]; then
  echo "fatal: nuclei failed" >&2
  exit 1
fi

export NUCLEI_VERSION="$(nuclei -version 2>&1 | grep Version: | cut -d':' -f2 | cut -d' ' -f2)"
echo '/nuclei-analyze > gl-dast-report.json' >&2
/nuclei-analyze > gl-dast-report.json
