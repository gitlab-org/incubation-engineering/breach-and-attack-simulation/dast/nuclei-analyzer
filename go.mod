module gitlab.com/gitlab-org/incubation-engineering/breach-and-attack-simulation/dast/nuclei-analyzer

go 1.19

require (
	github.com/santhosh-tekuri/jsonschema/v5 v5.1.1 // indirect
	golang.org/x/exp v0.0.0-20230129154200-a960b3787bd2 // indirect
)
