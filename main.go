package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"gitlab.com/gitlab-org/incubation-engineering/breach-and-attack-simulation/dast/nuclei-analyzer/parsehttp"
	"gitlab.com/gitlab-org/incubation-engineering/breach-and-attack-simulation/dast/nuclei-analyzer/schemas"
	"log"
	"os"
	"reflect"
	"strconv"
	"strings"
	"time"
)

const nucleiAnalyzerVersion = "0.0.1"
const gitlabVersion = "15.0.2"

func mapHeaders(h []parsehttp.HTTPHeader) []schemas.DastHeader {
	headers := []schemas.DastHeader{}
	for _, h := range h {
		headers = append(headers, schemas.DastHeader{Name: h.Name, Value: h.Value})
	}
	return headers
}

func nucleiToDastTimestamp(n string) (string, error) {
	t, err := time.Parse("2006-01-02T15:04:05.99Z", n)
	return t.Format("2006-01-02T15:04:05"), err
}

func Analyze(path, nucleiVersion string) ([]byte, error) {
	debug := false
	if env, ok := os.LookupEnv("NUCLEI_ANALYZER_DEBUG"); ok {
		var err error
		debug, err = strconv.ParseBool(env)
		if err != nil {
			log.Fatal(err)
		}
	}

	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(file)
	var vulns []schemas.DastVulnerability
	var scannedResources []schemas.DastScanScannedResource
	var firstResult schemas.NucleiResult
	var lastResult schemas.NucleiResult
	for scanner.Scan() {
		result := schemas.NucleiResult{}
		line := scanner.Text()
		err := json.Unmarshal([]byte(line), &result)
		if err != nil {
			if _, ok := err.(*json.SyntaxError); ok {
				// nuclei's JSONL output can be dirty if the -silent flag wasn't used.
				continue
			}

			if debug {
				log.Printf("fatal error parsing nuclei results: %+v\n", reflect.TypeOf(err))
			}
			log.Fatal(err)
		}

		if reflect.ValueOf(firstResult).IsZero() {
			firstResult = result
		}
		lastResult = result

		var ids []schemas.DastIdentifier
		// TODO: Iterate over all CWE, CVE, etc. on top of template ID.
		ids = append(ids, schemas.DastIdentifier{
			Type:  "NucleiCheck",
			Name:  fmt.Sprintf("nuclei-template::%s", result.TemplateID),
			URL:   fmt.Sprintf("https://gitlab.com/gitlab-org/incubation-engineering/breach-and-attack-simulation/dast/nuclei-analyzer/-/blob/main/fixtures/nuclei-templates/%s.yaml", result.TemplateID),
			Value: fmt.Sprintf("nuclei-template::%s", result.TemplateID),
		})

		var cve string
		if result.Info.Classification.CveID == nil {
			cve = ""
		} else {
			cve = result.Info.Classification.CveID.(string)
		}

		res, err := parsehttp.ParseResponse(result.Response)
		if err != nil {
			log.Fatal(err)
		}

		req, err := parsehttp.ParseRequest(result.Request)
		if err != nil {
			log.Fatal(err)
		}

		vuln := schemas.DastVulnerability{
			Assets:   []schemas.DastAsset{},
			Category: "DAST",
			Cve:      cve,
			Details: schemas.DastVulnerabilityDetails{
				"interactsh-summary": map[string]any{
					"name":  "Interactsh (Out-of-bound testing)",
					"type":  "value",
					"value": "The following details are from interacting with Interactsh proving ability to exfiltrate data.",
				},
				"interaction": map[string]any{
					"name": "Interactsh Interaction Details",
					"type": "table",
					"header": []map[string]string{
						map[string]string{"type": "text", "value": "Protocol"},
						map[string]string{"type": "text", "value": "Remote Address"},
						map[string]string{"type": "text", "value": "Timestamp"},
						map[string]string{"type": "text", "value": "Unique ID"},
					},
					"rows": [][]map[string]string{
						[]map[string]string{
							map[string]string{"type": "text", "value": result.Interaction.Protocol},
							map[string]string{"type": "text", "value": result.Interaction.RemoteAddress},
							map[string]string{"type": "text", "value": result.Interaction.Timestamp},
							map[string]string{"type": "text", "value": result.Interaction.UniqueID},
						},
					},
				},
				"interactsh-request": map[string]any{
					"name":  "Interactsh Request",
					"type":  "code",
					"lang":  "html",
					"value": result.Interaction.RawRequest,
				},
				"interactsh-response": map[string]any{
					"name":  "Interactsh Response",
					"type":  "code",
					"lang":  "html",
					"value": result.Interaction.RawResponse,
				},
			},
			Evidence: schemas.DastEvidence{
				Summary: fmt.Sprintf("Matcher returned true at %s", result.MatchedAt),
				Request: schemas.DastEvidenceRequest{
					Body:    req.Body,
					Headers: mapHeaders(req.Headers),
					Method:  req.Method,
					URL:     result.MatchedAt,
				},
				Response: schemas.DastEvidenceResponse{
					Body:         res.Body,
					Headers:      mapHeaders(res.Headers),
					ReasonPhrase: res.StatusText,
					StatusCode:   res.StatusCode,
				},
			},
			ID:          fmt.Sprintf("nuclei-template::%s", result.TemplateID),
			Identifiers: ids,
			Links:       []schemas.DastVulnerabilityLink{},
			Location: schemas.DastLocation{
				Hostname: result.Host,
				Method:   req.Method,
				Path:     req.Path,
			},
			Message: result.Info.Name,
			Name:    fmt.Sprintf("nuclei-template::%s", result.TemplateID),
			Scanner: schemas.DastScanner{
				ID:   "nuclei",
				Name: "nuclei",
			},
			Description: result.Info.Description,
			// TODO: Process nuclei severities => DAST report severity.
			Severity: strings.Title(result.Info.Severity),
		}
		scannedResources = append(scannedResources, schemas.DastScanScannedResource{
			Method: req.Method,
			URL:    result.MatchedAt,
			Type:   "url",
		})
		vulns = append(vulns, vuln)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	startTime, err := nucleiToDastTimestamp(firstResult.Timestamp)
	if err != nil {
		log.Fatal(err)
	}

	endTime, err := nucleiToDastTimestamp(lastResult.Timestamp)
	if err != nil {
		log.Fatal(err)
	}
	r := schemas.DastReport{
		Remediations: []interface{}{},
		Scan: schemas.DastScan{
			Messages:  []interface{}{},
			StartTime: startTime,
			EndTime:   endTime,
			Analyzer: schemas.DastScanAnalyzer{
				ID:   "nuclei-analyzer",
				Name: "Nuclei Analyzer",
				URL:  "https://gitlab.com/gitlab-org/incubation-engineering/breach-and-attack-simulation/dast/nuclei-analyzer",
				Vendor: schemas.DastScanAnalyzerVendor{
					Name: "GitLab",
				},
				Version: nucleiAnalyzerVersion,
			},
			Scanner: schemas.DastScanScanner{
				ID:      "nuclei",
				Name:    "nuclei",
				URL:     "https://nuclei.projectdiscovery.io",
				Version: nucleiVersion,
				Vendor: schemas.DastScanScannerVendor{
					Name: "nuclei",
				},
			},
			Status:           "success",
			Type:             "dast",
			ScannedResources: scannedResources,
		},
		Version:         gitlabVersion,
		Vulnerabilities: vulns,
	}
	json, err := json.MarshalIndent(r, "", " ")
	return json, err
}

func main() {
	nucleiVersion, ok := os.LookupEnv("NUCLEI_VERSION")
	if !ok {
		log.Fatalf("NUCLEI_VERSION must be set")
	}

	path, ok := os.LookupEnv("NUCLEI_ANALYZER_INPUT")
	if !ok {
		path = "./nuclei-results.json"
	}
	json, err := Analyze(path, nucleiVersion)
	if err != nil {
		log.Fatalf("error analyzing results: %#v", err)
	}
	fmt.Println(string(json))
}
