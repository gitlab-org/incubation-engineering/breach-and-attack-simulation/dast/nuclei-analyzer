package main

import (
	"encoding/json"
	"github.com/santhosh-tekuri/jsonschema/v5"
	_ "github.com/santhosh-tekuri/jsonschema/v5/httploader"
	"log"
	"testing"
)

func Test_Analyze(t *testing.T) {
	schema, err := jsonschema.Compile("https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/raw/v15.0.2/dist/dast-report-format.json")
	if err != nil {
		log.Fatalf("error compiling schema: %#v", err)
	}

	j, err := Analyze("fixtures/nuclei-results.json", "2.8.3")
	var raw interface{}
	if err = json.Unmarshal(j, &raw); err != nil {
		log.Fatalf("error unmarshalling data: %#v", err)
	}

	if err = schema.Validate(raw); err != nil {
		log.Fatalf("error validating schema: %#v", err)
	}
}
