package parsehttp

import (
	"bufio"
	"errors"
	"regexp"
	"strconv"
	"strings"
)

type HTTPHeader struct {
	Name  string
	Value string
}

type Response struct {
	Body       string
	Headers    []HTTPHeader
	StatusCode int
	StatusText string
}

type Request struct {
	Body    string
	Headers []HTTPHeader
	Method  string
	Path    string
}

func ParseResponse(raw string) (*Response, error) {
	res := Response{
		Headers: []HTTPHeader{},
	}
	s := bufio.NewScanner(strings.NewReader(raw))
	pastHeaders := false
	for s.Scan() {
		line := s.Text()
		if err := s.Err(); err != nil {
			return nil, err
		}

		// The start line will define the header, path, etc.
		if res.StatusCode == 0 {
			re := regexp.MustCompile(`HTTP/.*\s+(\d+)\s*(.*)`)
			match := re.FindStringSubmatch(line)
			if match == nil {
				return nil, errors.New("Invalid HTTP response start line")
			}

			code, err := strconv.Atoi(match[1])
			if err != nil {
				return nil, err
			}

			res.StatusCode = code
			// TODO: Support no status text and default to a sensible value?
			if match[2] != "" {
				res.StatusText = match[2]
			}
			continue
		}

		// If we're encountering a empty line we've past the HTTP headers section.
		// We may also end on a blank line if no body is provided.
		if line == "" {
			pastHeaders = true
			continue
		}

		if pastHeaders {
			// TODO: Better support multi-part resuests by retaining newlines.
			res.Body += line + "\n"
			continue
		}

		// Determine this HTTP header's name/value.
		parts := strings.SplitN(line, ":", 2)
		if len(parts) == 2 {
			res.Headers = append(res.Headers, HTTPHeader{
				Name:  parts[0],
				Value: strings.TrimSpace(parts[1]),
			})
		}
	}
	return &res, nil
}

func ParseRequest(raw string) (*Request, error) {
	req := Request{
		Headers: []HTTPHeader{},
	}
	s := bufio.NewScanner(strings.NewReader(raw))
	pastHeaders := false
	for s.Scan() {
		line := s.Text()
		if err := s.Err(); err != nil {
			return nil, err
		}

		// The start line will define the header, path, etc.
		if req.Method == "" {
			// 1: HTTP method
			// 2: Target
			re := regexp.MustCompile(`(CONNECT|DELETE|GET|HEAD|OPTIONS|PATCH|POST|PUT|TRACE)\s+(.*)\s+HTTP/.*`)
			match := re.FindStringSubmatch(line)
			if match == nil {
				return nil, errors.New("Invalid HTTP request start line")
			}

			req.Method = match[1]
			req.Path = match[2]
			continue
		}

		// If we're encountering a empty line we've past the HTTP headers section.
		// We may also end on a blank line if no body is provided.
		if line == "" {
			pastHeaders = true
			continue
		}

		if pastHeaders {
			// TODO: Better support multi-part requests by retaining newlines.
			req.Body += line + "\n"
			continue
		}

		// Determine this HTTP header's name/value.
		parts := strings.SplitN(line, ":", 2)
		if len(parts) == 2 {
			req.Headers = append(req.Headers, HTTPHeader{
				Name:  parts[0],
				Value: strings.TrimSpace(parts[1]),
			})
		}
	}

	return &req, nil
}
