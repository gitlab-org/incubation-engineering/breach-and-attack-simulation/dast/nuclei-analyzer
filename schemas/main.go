package schemas

type DastAsset struct {
	Type string `json:"type"`
	Name string `json:"name"`
	URL  string `json:"url"`
}

type DastIdentifier struct {
	Type  string `json:"type"`
	Name  string `json:"name"`
	URL   string `json:"url"`
	Value string `json:"value"`
}

type DastLocation struct {
	Hostname string `json:"hostname"`
	Method   string `json:"method"`
	Param    string `json:"param"`
	Path     string `json:"path"`
}

type DastScanAnalyzerVendor struct {
	Name string `json:"name"`
}

type DastScanAnalyzer struct {
	ID      string                 `json:"id"`
	Name    string                 `json:"name"`
	URL     string                 `json:"url",omitempty`
	Vendor  DastScanAnalyzerVendor `json:"vendor"`
	Version string                 `json:"version"`
}

type DastScanScannerVendor struct {
	Name string `json:"name"`
}

type DastScanScanner struct {
	ID      string                `json:"id"`
	Name    string                `json:"name"`
	URL     string                `json:"url"`
	Version string                `json:"version"`
	Vendor  DastScanScannerVendor `json:"vendor"`
}

type DastScanScannedResource struct {
	Method string `json:"method"`
	URL    string `json:"url"`
	Type   string `json:"type"`
}

type DastScan struct {
	Analyzer         DastScanAnalyzer          `json:"analyzer"`
	EndTime          string                    `json:"end_time"`
	Messages         []interface{}             `json:"messages"`
	ScannedResources []DastScanScannedResource `json:"scanned_resources"`
	Scanner          DastScanScanner           `json:"scanner"`
	StartTime        string                    `json:"start_time"`
	Status           string                    `json:"status"`
	Type             string                    `json:"type"`
}

type DastScanner struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type DastEvidenceSource struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type DastHeader struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

type DastEvidenceRequest struct {
	Body    string       `json:"body"`
	Headers []DastHeader `json:"headers"`
	Method  string       `json:"method"`
	URL     string       `json:"url"`
}

type DastEvidenceResponse struct {
	Body         string       `json:"body"`
	Headers      []DastHeader `json:"headers"`
	ReasonPhrase string       `json:"reason_phrase"`
	StatusCode   int          `json:"status_code"`
}

type DastEvidenceSupportingMessageRequest struct {
	Headers []DastHeader `json:"headers"`
	Method  string       `json:"method"`
	URL     string       `json:"url"`
	Body    string       `json:"body"`
}

type DastEvidenceSupportingMessageResponse struct {
	Headers      []DastHeader `json:"headers"`
	ReasonPhrase string       `json:"reason_phrase"`
	StatusCode   int          `json:"status_code"`
	Body         string       `json:"body"`
}

type DastEvidenceSupportingMessage struct {
	Name     string                                `json:"name"`
	Request  DastEvidenceSupportingMessageRequest  `json:"request"`
	Response DastEvidenceSupportingMessageResponse `json:"response,omitempty"`
}

type DastEvidence struct {
	Summary  string               `json:"summary"`
	Request  DastEvidenceRequest  `json:"request"`
	Response DastEvidenceResponse `json:"response"`
}

type DastVulnerabilityLink struct {
	URL string `json:"url"`
}

type DastVulnerabilityDetails map[string]interface{}

type DastVulnerability struct {
	Assets      []DastAsset              `json:"assets"`
	Category    string                   `json:"category"`
	Cve         string                   `json:"cve"`
	Description string                   `json:"description"`
	Details     DastVulnerabilityDetails `json:"details,omitempty"`
	Evidence    DastEvidence             `json:"evidence"`
	ID          string                   `json:"id"`
	Identifiers []DastIdentifier         `json:"identifiers"`
	Location    DastLocation             `json:"location"`
	Links       []DastVulnerabilityLink  `json:"links"`
	Message     string                   `json:"message"`
	Name        string                   `json:"name"`
	Scanner     DastScanner              `json:"scanner"`
	Severity    string                   `json:"severity"`
	Solution    string                   `json:"solution"`
}

// See also https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/v15.0.2/dist/dast-report-format.json
type DastReport struct {
	Scan            DastScan            `json:"scan"`
	Version         string              `json:"version"`
	Vulnerabilities []DastVulnerability `json:"vulnerabilities"`
	Remediations    []interface{}       `json:"remediations"`
}

type NucleiResult struct {
	Template    string `json:"template"`
	TemplateURL string `json:"template-url"`
	TemplateID  string `json:"template-id"`
	Info        struct {
		Name           string   `json:"name"`
		Author         []string `json:"author"`
		Tags           []string `json:"tags"`
		Description    string   `json:"description"`
		Reference      []string `json:"reference"`
		Severity       string   `json:"severity"`
		Classification struct {
			CveID interface{} `json:"cve-id"`
			CweID []string    `json:"cwe-id"`
		} `json:"classification"`
	} `json:"info"`
	Interaction struct {
		FullID        string `json:"full-id"`
		Protocol      string `json:"protocol"`
		RawRequest    string `json:"raw-request"`
		RawResponse   string `json:"raw-response"`
		RemoteAddress string `json:"remote-address"`
		Timestamp     string `json:"timestamp"`
		UniqueID      string `json:"unique-id"`
	} `json:"interaction"`
	Type      string `json:"type"`
	Host      string `json:"host"`
	MatchedAt string `json:"matched-at"`
	Meta      struct {
		Username string `json:"username"`
		Password string `json:"password"`
	} `json:"meta"`
	IP            string      `json:"ip"`
	Timestamp     string      `json:"timestamp"`
	CurlCommand   string      `json:"curl-command"`
	MatcherStatus bool        `json:"matcher-status"`
	MatchedLine   interface{} `json:"matched-line"`
	Request       string      `json:"request"`
	Response      string      `json:"response"`
	// TODO: Add Interaction type
}
