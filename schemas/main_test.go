package schemas

import (
	"encoding/json"
	"github.com/santhosh-tekuri/jsonschema/v5"
	_ "github.com/santhosh-tekuri/jsonschema/v5/httploader"
	"log"
	"testing"
)

func Test_DastReportInterfaceMarshalsAppropriately(t *testing.T) {
	schema, err := jsonschema.Compile("https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/raw/v15.0.2/dist/dast-report-format.json")
	if err != nil {
		log.Fatalf("error compiling schema: %#v", err)
	}

	v := DastReport{
		Remediations: []interface{}{},
		Scan: DastScan{
			Analyzer: DastScanAnalyzer{
				ID:   "gitlab-dast",
				Name: "GitLab DAST",
				URL:  "https://gitlab.com/gitlab-org/security-products/security-report-schemas",
				Vendor: DastScanAnalyzerVendor{
					Name: "GitLab",
				},
				Version: "3.0.56",
			},
			EndTime:  "2023-01-16T15:07:35",
			Messages: []interface{}{},
			ScannedResources: []DastScanScannedResource{
				{Method: "GET", Type: "url", URL: "http://dvwa"},
				{Method: "GET", Type: "url", URL: "http://dvwa/"},
				{Method: "GET", Type: "url", URL: "http://dvwa/about.php"},
				{Method: "GET", Type: "url", URL: "http://dvwa/docs/DVWA_v1.3.pdf"},
				{Method: "GET", Type: "url", URL: "http://dvwa/dvwa/css/main.css"},
				{Method: "GET", Type: "url", URL: "http://dvwa/dvwa/images/logo.png"},
				{Method: "GET", Type: "url", URL: "http://dvwa/dvwa/images/spanner.png"},
				{Method: "GET", Type: "url", URL: "http://dvwa/dvwa/js/dvwaPage.js"},
				{Method: "GET", Type: "url", URL: "http://dvwa/favicon.ico"},
				{Method: "GET", Type: "url", URL: "http://dvwa/instructions.php"},
				{Method: "GET", Type: "url", URL: "http://dvwa/instructions.php?doc=PDF"},
				{Method: "GET", Type: "url", URL: "http://dvwa/instructions.php?doc=PHPIDS-license"},
				{Method: "GET", Type: "url", URL: "http://dvwa/instructions.php?doc=changelog"},
				{Method: "GET", Type: "url", URL: "http://dvwa/instructions.php?doc=copying"},
				{Method: "GET", Type: "url", URL: "http://dvwa/instructions.php?doc=readme"},
				{Method: "GET", Type: "url", URL: "http://dvwa/login.php"},
				{Method: "GET", Type: "url", URL: "http://dvwa/robots.txt"},
				{Method: "GET", Type: "url", URL: "http://dvwa/setup.php"},
				{Method: "POST", Type: "url", URL: "http://dvwa/setup.php"},
				{Method: "GET", Type: "url", URL: "http://dvwa/sitemap.xml"},
				{Method: "GET", Type: "url", URL: "http://dvwa/var/www/html/config/config.inc.php"},
			},
			Scanner: DastScanScanner{
				ID:   "zaproxy",
				Name: "OWASP Zed Attack Proxy (ZAP)",
				URL:  "https://www.zaproxy.org",
				Vendor: DastScanScannerVendor{
					Name: "GitLab",
				},
				Version: "2.12.0",
			},
			StartTime: "2023-01-16T15:05:32",
			Status:    "success",
			Type:      "dast",
		},
		Version: "15.0.2",
		Vulnerabilities: []DastVulnerability{
			{
				Assets:      []DastAsset{},
				Description: "No Anti-CSRF tokens were found in a HTML submission form. A cross-site request forgery is an attack that involves forcing a victim to send an HTTP request to a target destination without their knowledge or intent in order to perform an action as the victim. The underlying cause is application functionality using predictable URL/form actions in a repeatable way. The nature of the attack is that CSRF exploits the trust that a web site has for a user. By contrast, cross-site scripting (XSS) exploits the trust that a user has for a web site. Like XSS, CSRF attacks are not necessarily cross-site, but they can be. Cross-site request forgery is also known as CSRF, XSRF, one-click attack, session riding, confused deputy, and sea surf. CSRF attacks are effective in a number of situations, including: * The victim has an active session on the target site. * The victim is authenticated via HTTP auth on the target site. * The victim is on the same local network as the target site. CSRF has primarily been used to perform an action against a target site using the victim's privileges, but recent techniques have been discovered to disclose information by gaining access to the response. The risk of information disclosure is dramatically increased when the target site is vulnerable to XSS, because XSS can be used as a platform for CSRF, allowing the attack to operate within the bounds of the same-origin policy.",
				Evidence: DastEvidence{
					Request: DastEvidenceRequest{
						Headers: []DastHeader{
							{Name: "Accept", Value: "*/*"},
							{Name: "Connection", Value: "keep-alive"},
							{Name: "Cookie", Value: "PHPSESSID=********; security=********"},
							{Name: "Host", Value: "dvwa"},
							{Name: "User-Agent", Value: "python-requests/2.26.0"},
						},
						Method: "GET",
						URL:    "http://dvwa/setup.php",
					},
					Response: DastEvidenceResponse{
						Headers: []DastHeader{
							{Name: "Cache-Control", Value: "no-cache, must-revalidate"},
							{Name: "Connection", Value: "Keep-Alive"},
							{Name: "Content-Length", Value: "3451"},
							{Name: "Content-Type", Value: "text/html;charset=utf-8"},
							{Name: "Date", Value: "Mon, 16 Jan 2023 15:05:50 GMT"},
							{Name: "Expires", Value: "Tue, 23 Jun 2009 12:00:00 GMT"},
							{Name: "Keep-Alive", Value: "timeout=5, max=98"},
							{Name: "Pragma", Value: "no-cache"},
							{Name: "Server", Value: "Apache/2.4.10 (Debian)"},
							{Name: "Vary", Value: "Accept-Encoding"},
						},
						ReasonPhrase: "OK",
						StatusCode:   200,
					},
					Summary: "<form action=\"#\" method=\"post\">; No known Anti-CSRF token [anticsrf, CSRFToken, __RequestVerificationToken, csrfmiddlewaretoken, authenticity_token, OWASP_CSRFTOKEN, anoncsrf, csrf_token, _csrf, _csrfSecret, __csrf_magic, CSRF, _token, _csrf_token] was found in the following HTML form: [Form 1: \"create_db\" \"user_token\" ].",
				},
				ID: "28f97f88-83ac-4f95-ba26-ca2b24e87981",
				Identifiers: []DastIdentifier{
					{
						Name:  "Absence of Anti-CSRF Tokens",
						Type:  "ZAProxy_PluginId",
						URL:   "https://github.com/zaproxy/zaproxy/blob/w2019-01-14/docs/scanners.md",
						Value: "10202",
					},
					{
						Name:  "CWE-352",
						Type:  "CWE",
						URL:   "https://cwe.mitre.org/data/definitions/352.html",
						Value: "352",
					},
				},
				Links: []DastVulnerabilityLink{
					{
						URL: "http://projects.webappsec.org/Cross-Site-Request-Forgery",
					},
					{
						URL: "http://cwe.mitre.org/data/definitions/352.html",
					},
				},
				Location: DastLocation{
					Hostname: "http://dvwa",
					Method:   "GET",
					Param:    "",
					Path:     "/setup.php",
				},
				Name:     "Absence of Anti-CSRF Tokens",
				Severity: "Medium",
				Solution: "Phase: Architecture and Design Use a vetted library or framework that does not allow this weakness to occur or provides constructs that make this weakness easier to avoid. For example, use anti-CSRF packages such as the OWASP CSRFGuard. Phase: Implementation Ensure that your application is free of cross-site scripting issues, because most CSRF defenses can be bypassed using attacker-controlled script. Phase: Architecture and Design Generate a unique nonce for each form, place the nonce into the form, and verify the nonce upon receipt of the form. Be sure that the nonce is not predictable (CWE-330). Note that this can be bypassed using XSS. Identify especially dangerous operations. When the user performs a dangerous operation, send a separate confirmation request to ensure that the user intended to perform that operation. Note that this can be bypassed using XSS. Use the ESAPI Session Management control. This control includes a component for CSRF. Do not use the GET method for any request that triggers a state change. Phase: Implementation Check the HTTP Referer header to see if the request originated from an expected page. This could break legitimate functionality, because users or proxies may have disabled sending the Referer for privacy reasons.",
			},
		},
	}

	// marshal type to json string then unmarshal into an interface so jsonschema can do it's job.
	j, _ := json.MarshalIndent(v, "", " ")
	var raw interface{}
	if err = json.Unmarshal(j, &raw); err != nil {
		log.Fatalf("error unmarshalling data: %#v", err)
	}

	if err = schema.Validate(raw); err != nil {
		log.Fatalf("error validating schema: %#v", err)
	}
}
